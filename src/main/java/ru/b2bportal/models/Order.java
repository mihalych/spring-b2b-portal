package ru.b2bportal.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "orders")
@Component
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "delivery_point_id")
    private DeliveryPoint deliveryPoint;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "order")
    protected List<OrderDetail> details;

    private String externalId;
    private Date date;
    private Date shippingDate;
    private Date deliveryDate;
    private BigDecimal sum;
    private BigDecimal volume;

}
