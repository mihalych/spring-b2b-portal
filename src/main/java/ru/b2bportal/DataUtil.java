package ru.b2bportal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.b2bportal.models.Client;
import ru.b2bportal.models.DeliveryPoint;
import ru.b2bportal.models.Product;
import ru.b2bportal.repositories.ClientsRepository;
import ru.b2bportal.repositories.DeliveryPointsRepository;
import ru.b2bportal.repositories.ProductsRepository;

import java.sql.*;

@Component
public class DataUtil {

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    DeliveryPointsRepository deliveryPointsRepository;

    @Autowired
    ProductsRepository productsRepository;

    private static final String url = "jdbc:mysql://localhost/primefruit?useSSL=false&allowMultiQueries=true&serverTimezone=UTC";
    private static final String user = "cuba";
    private static final String password = "87917010";

    public void initializeData() {

        //initializeClientData();

        /*for (Client client : clientsRepository.findAll()) {
            initializeDeliveryPointData(client);
        }*/

        //initializeProductData();

    }

    private void initializeProductData() {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();

            String query = "SELECT id, name, price, alcohol, kod_external FROM primefruit.primefruit_sku where enabled = '1'";

            rs = stmt.executeQuery(query);

            while (rs.next()) {

                Product product = Product.builder()
                        .name(rs.getString("name"))
                        .externalId(textToId(rs.getString("id")))
                        .cost(rs.getLong("price"))
                        .barcode(rs.getString("kod_external"))
                        .alcohol(rs.getLong("alcohol"))
                        .enabled(true)
                        .build();
                productsRepository.save(product);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
                stmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeDeliveryPointData(Client client) {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();

            String id = idToText(client.getExternalId());
            String query = "SELECT id, name, adress FROM primefruit.primefruit_delivery_points where owner_id = '" + id + "'";

            rs = stmt.executeQuery(query);

            while (rs.next()) {

                DeliveryPoint deliveryPoint = DeliveryPoint.builder()
                        .name(rs.getString("name"))
                        .externalId(textToId(rs.getString("id")))
                        .address(rs.getString("adress"))
                        .client(client)
                        .enabled(true)
                        .build();
                deliveryPointsRepository.save(deliveryPoint);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
                stmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeClientData() {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();

            String query = "SELECT id, name, adress, inn FROM primefruit.primefruit_owner where enabled = '1'";

            rs = stmt.executeQuery(query);

            while (rs.next()) {

                Client client = Client.builder()
                        .name(rs.getString("name"))
                        .externalId(textToId(rs.getString("id")))
                        .address(rs.getString("adress"))
                        .inn(rs.getString("inn"))
                        .enabled(true)
                        .build();
                clientsRepository.save(client);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
                stmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static String textToId(String id) {
        return id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16)+ "-" + id.substring(16, 20) + "-" + id.substring(20);
    }

    private static String idToText(String id) {
        return id.replaceAll("-", "").toLowerCase();
    }

}
