package ru.b2bportal.forms;

import lombok.Data;

@Data
public class OrderDetailForm {
    private String productName;
    private Integer quantity;
}
