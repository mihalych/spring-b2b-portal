package ru.b2bportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.b2bportal.services.ConfirmService;

@Controller
public class ConfirmController {

    @Autowired
    private ConfirmService confirmService;

    @GetMapping("/user/confirm/{confirm-code}")
    public String getConfirmPage(Model model, @PathVariable("confirm-code") String confirmCode) {
        boolean isConfirmed = confirmService.confirmUser(confirmCode);
        model.addAttribute("isConfirmed", isConfirmed);
        return "success_confirm_page";
    }

}
