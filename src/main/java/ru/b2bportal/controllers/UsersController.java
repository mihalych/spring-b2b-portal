package ru.b2bportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.b2bportal.models.Authority;
import ru.b2bportal.models.User;
import ru.b2bportal.repositories.UsersRepository;
import ru.b2bportal.services.ConfirmService;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UsersController {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    private ConfirmService confirmService;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public String getUserList(Model model) {
        model.addAttribute("users",usersRepository.findAll());
        return "user_list_page";
    }

    @GetMapping("{user}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String getUserEditPage(@PathVariable User user, Model model) {
        model.addAttribute("user",user);
        model.addAttribute("authority", Authority.values());
        return "user_edit_page";
    }

    @PostMapping
    public String userSave(@RequestParam String email,
                           @RequestParam String firstName,
                           @RequestParam String lastName,
                           @RequestParam("userId") User user,
                           @RequestParam Map<String, String> form) {
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);

        Set<String> roles = Arrays.stream(Authority.values())
                .map(Authority::name)
                .collect(Collectors.toSet());

        user.getAuthority().clear();
        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getAuthority().add(Authority.valueOf(key));
            }
        }

        usersRepository.save(user);

        return "redirect:/users";
    }
}
