package ru.b2bportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.b2bportal.forms.OrderDetailForm;
import ru.b2bportal.models.Order;
import ru.b2bportal.models.OrderDetail;
import ru.b2bportal.models.Product;
import ru.b2bportal.repositories.ClientsRepository;
import ru.b2bportal.repositories.DeliveryPointsRepository;
import ru.b2bportal.repositories.OrdersRepository;
import ru.b2bportal.security.details.UserDetailsImpl;
import ru.b2bportal.services.OrdersService;

import java.util.Arrays;

@Controller
@RequestMapping("/addNewOrder")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private DeliveryPointsRepository deliveryPointsRepository;

    @GetMapping
    public String getAddNewOrderPage(@AuthenticationPrincipal UserDetailsImpl usersDetails, Model model) {
        model.addAttribute("clients", clientsRepository.findAll());
        model.addAttribute("deliveryPoints", deliveryPointsRepository.findAll());
        return "addNewOrder_page";
    }

    @PostMapping
    public String addNewOrder(@AuthenticationPrincipal UserDetailsImpl usersDetails, OrderDetailForm orderDetailForm) {
        ordersService.addNewOrder(usersDetails.getUser(), orderDetailForm);
        return "redirect:/main";
    }

}
