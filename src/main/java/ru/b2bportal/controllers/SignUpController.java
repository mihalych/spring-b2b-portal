package ru.b2bportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.b2bportal.forms.UserForm;
import ru.b2bportal.services.SignUpService;

@Controller
public class SignUpController {

    @Autowired
    private SignUpService signUpService;

    @GetMapping("/signUp")
    public String getSignUpPage(Authentication authentication) {
        if (authentication == null) {
            return "sign_up_page";
        } else {
            return "redirect:/";
        }
    }

    @PostMapping("/signUp")
    public String signUpUser(UserForm userForm) {

        signUpService.signUp(userForm);

        return "redirect:/signIn";
    }
}
