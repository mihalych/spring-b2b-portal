package ru.b2bportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.b2bportal.models.Order;
import ru.b2bportal.models.User;
import ru.b2bportal.repositories.OrdersRepository;
import ru.b2bportal.security.details.UserDetailsImpl;

@Controller
@RequestMapping("/main")
public class MainController {

    @Autowired
    private OrdersRepository ordersRepository;

    @GetMapping
    public String getMainPage(@AuthenticationPrincipal UserDetailsImpl usersDetails, Model model) {
        /*User user = usersDetails.getUser();*/
        model.addAttribute("orders", ordersRepository.findAll());
        return "main_page";
    }
}
