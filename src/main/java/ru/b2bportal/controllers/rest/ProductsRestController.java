package ru.b2bportal.controllers.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.b2bportal.dto.ProductDto;
import ru.b2bportal.services.ProductsService;

import java.util.List;

@RestController
public class ProductsRestController {

    @Autowired
    private ProductsService productsService;

    @ApiOperation(value = "Поиск товаров")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Поиск завершился успешно"),
            @ApiResponse(code = 403, message = "Неверный токен авторизации")
    })
    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping("/products/search")
    public List<ProductDto> search(@RequestParam(value = "cost", required = false) Long cost,
                                @RequestParam(value = "page") Integer page,
                                @RequestParam(value = "size") Integer size) {
        return productsService.search(cost, page, size);
    }


    @ApiOperation(value = "Добавление товара")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар успешно добавлен"),
            @ApiResponse(code = 403, message = "Неверный токен авторизации")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/products/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<ProductDto> addProduct(@RequestBody ProductDto productDto) {

        productsService.saveNewProduct(productDto);
        return productsService.getAll();
    }

}
