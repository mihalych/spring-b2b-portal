package ru.b2bportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.b2bportal.dto.ProductDto;
import ru.b2bportal.models.Product;
import ru.b2bportal.repositories.ProductsRepository;
import ru.b2bportal.services.ProductsService;

import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @Autowired
    private ProductsRepository productsRepository;

    /*@RequestMapping(value = "/products", method = RequestMethod.POST)
    public String addProduct(Product product) {
        productsRepository.save(product);
        return "redirect:/products";
    }*/

    @GetMapping()
    public String getProductsPage(Model model, @PageableDefault(sort = {"name"}, direction = Sort.Direction.ASC) Pageable pageable) {
        /*List<ProductDto> products = productsService.getAll();*/
        model.addAttribute("page", productsRepository.findAll(pageable));
        model.addAttribute("url", "/products");
        return "products_page";
    }
}
