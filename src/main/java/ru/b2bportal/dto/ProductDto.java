package ru.b2bportal.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.b2bportal.models.Product;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto {
    private Long id;
    private String name;
    private Long cost;

    public static ProductDto from(Product product) {
        return ProductDto.builder()
                .name(product.getName())
                .cost(product.getCost())
                .id(product.getId())
                .build();
    }

    public static List<ProductDto> from(List<Product> productList) {
        return productList.stream()
                .map(ProductDto::from)
                .collect(Collectors.toList());
    }

}
