package ru.b2bportal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.b2bportal.dto.ProductDto;
import ru.b2bportal.models.Product;
import ru.b2bportal.repositories.ProductsRepository;

import java.util.List;

@Service
public class ProductsServiceImpl implements ProductsService {

    @Autowired
    ProductsRepository productsRepository;

    @Override
    public List<ProductDto> search(Long cost, Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("id").descending());
        return ProductDto.from(productsRepository.findProductByCostIsLessThanEqual(cost, pageRequest));
    }

    @Override
    public List<ProductDto> getAll() {
        return productsRepository.findAllWithoutSubEntitiesAsDto();
    }

    @Override
    public boolean saveNewProduct(ProductDto productDto) {
        Product newProduct = Product.builder()
                .name(productDto.getName())
                .cost(productDto.getCost())
                .build();
        productsRepository.save(newProduct);
        return true;
    }
}
