package ru.b2bportal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.b2bportal.forms.OrderDetailForm;
import ru.b2bportal.models.Order;
import ru.b2bportal.models.OrderDetail;
import ru.b2bportal.models.Product;
import ru.b2bportal.models.User;
import ru.b2bportal.repositories.OrdersRepository;
import ru.b2bportal.repositories.ProductsRepository;

import java.util.Arrays;
import java.util.Optional;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Override
    public boolean addNewOrder(User user, OrderDetailForm orderDetailForm) {

        Optional<Product> productOptional = productsRepository.findProductByName(orderDetailForm.getProductName());
        if (productOptional.isPresent()) {
            /*Order newOrder = Order.builder()
                    .user(user)
                    //.details(Arrays.asList(orderDetail))
                    .build();

            OrderDetail orderDetail = OrderDetail.builder()
                    .product(productOptional.get())
                    .quantity(orderDetailForm.getQuantity())
                    .order(newOrder)
                    .build();

            newOrder.setDetails(Arrays.asList(orderDetail));
            ordersRepository.save(newOrder);*/
            return true;
        }
        return false;
    }
}
