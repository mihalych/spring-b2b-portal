package ru.b2bportal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.b2bportal.models.State;
import ru.b2bportal.models.User;
import ru.b2bportal.repositories.UsersRepository;

import java.util.Optional;

@Service
public class ConfirmServiceImpl implements ConfirmService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean confirmUser(String confirmCode) {
        Optional<User> userOptional = usersRepository.findByConfirmCode(confirmCode);

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setState(State.CONFIRM);
            usersRepository.save(user);
            return true;
        }

        return false;
    }
}
