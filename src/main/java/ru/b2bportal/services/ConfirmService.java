package ru.b2bportal.services;

public interface ConfirmService {
    boolean confirmUser(String confirmCode);
}
