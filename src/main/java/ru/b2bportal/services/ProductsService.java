package ru.b2bportal.services;

import ru.b2bportal.dto.ProductDto;

import java.util.List;

public interface ProductsService {

    List<ProductDto> search(Long cost, Integer page, Integer size);
    List<ProductDto> getAll();
    boolean saveNewProduct(ProductDto productDto);

}
