package ru.b2bportal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.b2bportal.forms.UserForm;
import ru.b2bportal.models.Authority;
import ru.b2bportal.models.State;
import ru.b2bportal.models.User;
import ru.b2bportal.repositories.UsersRepository;

import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String userName;

    private ExecutorService executorService = Executors.newCachedThreadPool();

    @Override
    public void signUp(UserForm userForm) {

        User user = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .email(userForm.getEmail())
                .hashPassword(passwordEncoder.encode(userForm.getPassword()))
                .confirmCode(UUID.randomUUID().toString())
                .state(State.NOT_CONFIRM)
                .authority(Collections.singleton(Authority.USER))
                .build();

        executorService.submit(() -> {
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setFrom(userName);
                messageHelper.setTo(user.getEmail());
                messageHelper.setSubject("Confirm Registration");
                messageHelper.setText("http://localhost/users/confirm/" + user.getConfirmCode(), true);
            };

            javaMailSender.send(messagePreparator);
        });


        usersRepository.save(user);

    }
}
