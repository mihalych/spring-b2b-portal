package ru.b2bportal.services;

import ru.b2bportal.forms.UserForm;

public interface SignUpService {
    void signUp(UserForm userForm);
}
