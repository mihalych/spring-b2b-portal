package ru.b2bportal.services;

import ru.b2bportal.forms.OrderDetailForm;
import ru.b2bportal.models.User;

public interface OrdersService {
    boolean addNewOrder(User user, OrderDetailForm orderDetailForm);
}
