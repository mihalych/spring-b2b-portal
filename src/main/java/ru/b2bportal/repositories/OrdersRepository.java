package ru.b2bportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.b2bportal.models.Client;
import ru.b2bportal.models.Order;
import ru.b2bportal.models.User;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByClient(Client client);
}
