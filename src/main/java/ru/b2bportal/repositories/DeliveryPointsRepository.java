package ru.b2bportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.b2bportal.models.DeliveryPoint;

public interface DeliveryPointsRepository extends JpaRepository<DeliveryPoint, Long> {
}
