package ru.b2bportal.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.b2bportal.dto.ProductDto;
import ru.b2bportal.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository extends CrudRepository<Product, Long> {

    @Query("select new ru.b2bportal.dto.ProductDto(product.id, product.name, product.cost) from Product product " +
            "group by product.id, product.cost")
    List<ProductDto> findAllWithoutSubEntitiesAsDto();

    Page<Product> findAll(Pageable pageable);
    Optional<Product> findProductByName(String name);
    List<Product> findProductByCostIsLessThanEqual(Long cost, Pageable pageable);
}
