package ru.b2bportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.b2bportal.models.Client;

public interface ClientsRepository extends JpaRepository<Client, Long> {

}
